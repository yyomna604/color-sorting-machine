#define dirPin 7
#define stepPin 8
#define high 4
#define stepsPerRevolution 200

void setup() {

  // initialize serial:

  Serial.begin(9600);
  digitalWrite(high, HIGH);
  pinMode(stepPin, OUTPUT);
  pinMode(dirPin, OUTPUT);
}
String col;
String val;
char degORstep;
String dir;
int val_int;
int dir_int;
char direct = 'l';
int v_int;
String sam = "left";

void rotateMotor(int val_int, int dir_int)
{
  // Set the spinning direction clockwise:
  if (dir_int == 0){
    digitalWrite(dirPin, HIGH);
  }
  else if (dir_int == 1){
    digitalWrite(dirPin, LOW);
  }
  // Spin the stepper motor 1 revolution slowly:
  for (int i = 0; i < val_int*stepsPerRevolution/2; i++) {
  // These four lines result in 1 step:
  digitalWrite(stepPin, HIGH);
  delayMicroseconds(2000);
  digitalWrite(stepPin, LOW);
  delayMicroseconds(2000);
}
}

void loop() {
  while (Serial.available() > 0) {
    col = Serial.readString();// read the incoming data as string
    dir = col.substring(col.length()-3, col.length());
    val = col.substring(0, col.length()-3);
    val_int = val.toInt();
    v_int = dir.toInt();
    Serial.print(v_int);
    if(v_int == int(10))
      {
        dir_int = 0;
      }
    else if(v_int == int(20))
      {
        dir_int = 1;
      }
    rotateMotor(val_int, dir_int);
    }
  }
